# Instructions for testing ThinLinc

## If you are a Windows 10 user

1. Follow the instruction to [install and configure Windows Subsystem for Linux (WSL)](https://www.pdc.kth.se/support/documents/login/windows_login.html#wsl-approach).
   We recommend the "Ubuntu 18.04 LTS" Linux distribution.

1. Install an X-server ([Xming](https://sourceforge.net/projects/xming/) or [vcXsrv](https://sourceforge.net/projects/vcxsrv/)). Start the X-server after installation.
 
1. Open WSL terminal, create file ``/etc/fonts/local.conf`` and add the following lines (need sudo privilege)
   ```
   <?xml version="1.0"?>
   <!DOCTYPE fontconfig SYSTEM "fonts.dtd">
   <fontconfig>
       <dir>/mnt/c/Windows/Fonts</dir>
   </fontconfig>
   ```

1. Install ImageMagick along with heimdal-clients and openssh-client (taking Ubuntu for example)
   ```
   sudo apt install imagemagick
   sudo apt install heimdal-clients
   sudo apt install openssh-client
   ```

1. Add the following line to ``~/.bashrc``
   ```
   export DISPLAY=:0
   ```

## Setting up your local computer (WSL/Linux/MacOS)

1. Download and install [ThinLinc client](https://www.cendio.com/thinlinc/download). Note that WSL user should download Linux client and install the client within WSL.

   If you are using Ubuntu, use the following command to install
   ```
   sudo dpkg -i ./thinlinc-client_4.10.1-6197_amd64.deb
   ```

1. Update ``~/.thinlinc/config`` with the following line
   ```
   GSSAPIDelegateCredentials  yes
   ```

1. Make sure that ``~/.thinlinc/config`` has correct permission
   ```
   chmod 644 ~/.thinlinc/config
   ```

1. Update ``/etc/krb5.conf`` with the following lines
   ```
   [domain_realm]
     .pdc.kth.se = NADA.KTH.SE
   [appdefaults]
     forwardable = yes
     forward = yes
     krb4_get_tickets = no
   [libdefaults]
     default_realm = NADA.KTH.SE
     dns_lookup_realm = true
     dns_lookup_kdc = true
   ```

## Connecting to ThinLinc login node

1. Get Kerberot ticket on your local computer (WSL/Linux/MacOS)
   ```
   kinit -f <username>@NADA.KTH.SE
   ```

1. Start ThinLinc client (WSL user should start X-server first).
   
   If you use WSL or Linux, use the following command to start ThinLinc.
   ```
   /opt/thinlinc/bin/tlclient &
   ```

1. In ThinLinc client:
   + Server: ``t03n33.pdc.kth.se``
   + Username: your-username
   + Options -> Screen -> choose "Work area (maximized)" under "Size of session" and uncheck "Full screen mode"
   + Options -> Security -> choose "Kerberos ticket" under "Authentication method"
   + Click "OK" and then "Connect"
   + If this is the first time you connect to ``t03n33.pdc.kth.se``, click "Continue"
   + After the "Welcome to Cendio Thinlinc" window pops up, click "OK"
   + If this is the first time you log in to ``t03n33.pdc.kth.se``, click "Use default config"

## After logging in to ThinLinc login node

1. Update background image (copy-paste the following command in terminal)
   ```
   xfconf-query --channel xfce4-desktop \
                --property /backdrop/screen0/monitor0/workspace0/last-image \
                --set /usr/share/backgrounds/pdc.png
   ```

1. Try vmd
   ```
   export PATH=/cfs/klemming/nobackup/l/lxin/vmd/1.9.3/bin:$PATH
   vmd ...
   ```

1. If you would like to work with your jobs on Beskow, double-click the "Beskow" shortcut on your desktop.

## Making use of GPU on Tegner

1. Use the following submission script to request a GPU node for e.g. an hour

   ```
   #!/bin/bash

   #SBATCH -A 201X-X-XX
   #SBATCH -J vgltest
   #SBATCH -t 00:59:00

   #SBATCH --nodes=1
   #SBATCH --ntasks-per-node=24
   #SBATCH --gres=gpu:K420:1

   set -x

   XDISPLAY=:0
   XVNCDISPLAY=:1
   export DISPLAY=$XDISPLAY
   Xorg $XDISPLAY -nolisten tcp &
   XORGPID=$!
   # Wait for Xorg to come up:
   sleep 1s
   for t in 1 1 2 2 4 4 4 8 8 8 8 16; do
       # If xset works against the server we assume its up
       if xset -display $XDISPLAY  q > /dev/null; then
       break;
       else
       echo Waiting for Xorg server to come up 1>&2
       sleep $t
       fi
   done
   if ! xset -display $XDISPLAY  q > /dev/null; then
       # Not up after exit of loop - exit script with error message
       echo "Backing X-server did not seem to start properly - exiting..." 1>&2
       exit 1;
   fi

   wait
   ```

1. Use ``squeue -u <username>`` to find out the allocated compute node. Connect to compute node via ``vglconnect``
   ```
   vglconnect -s <username>@t0XnXX.pdc.kth.se
   ```

1. After ``vglconnect`` one can run programs on the GPU via ``vglrun``
   
   1. glxgears
      ```
      vglrun glxgears
      ```

   1. VMD
      ```
      export PATH=/cfs/klemming/nobackup/l/lxin/vmd/1.9.3/bin:$PATH
      vglrun vmd ...
      ```

   1. Matlab
      ```
      module load matlab
      vglrun matlab -nosoftwareopengl
      ```

   1. Paraview
      ```
      wget http://web.engr.oregonstate.edu/~mjb/paraview/terrain.csv
      wget http://web.engr.oregonstate.edu/~mjb/paraview/terrain.pvsm
      module load paraview/5.2.0-gcc-6.2
      vglrun paraview
      ```