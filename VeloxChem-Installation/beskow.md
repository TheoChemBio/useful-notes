# Beskow

### Installation of VeloxChem

```
module load mpi4py/3.0.2/py37
module swap PrgEnv-cray PrgEnv-intel

export CRAYPE_LINK_TYPE=dynamic
export CRAY_ROOTFS=DSL
export CXX=CC

mkdir -p $SNIC_NOBACKUP/software
cd $SNIC_NOBACKUP/software

git clone https://gitlab.com/rinkevic/VeloxChemMP.git
cd VeloxChemMP
python3 config/generate_setup.py

export PATH=$SNIC_NOBACKUP/software/VeloxChemMP/build/bin:$PATH
export PYTHONPATH=$SNIC_NOBACKUP/software/VeloxChemMP/build/python:$PYTHONPATH

salloc -N 1 -t 30 -A 2019-3-203
OMP_NUM_THREADS=32 srun -n 1 python3 setup.py build
# The following error message is expected and can be ignored:
# PermissionError: [Errno 13] Permission denied: '/afs'

OMP_NUM_THREADS=32 srun -n 1 pytest -v python_tests
OMP_NUM_THREADS=16 srun -n 2 pytest -v python_tests
```

### Job submission script

**_Note_**: _Please do NOT use the "--cpus-per-task" option on Beskow._

```
#!/bin/bash

#SBATCH -A 201X-X-XXX
#SBATCH -J tio2
#SBATCH -t 23:59:00

#SBATCH --nodes=32
#SBATCH --ntasks-per-node=1
#SBATCH -C Haswell

# mpi4py will load intel and anaconda
module load mpi4py/3.0.2/py37
module swap PrgEnv-cray PrgEnv-intel
export CRAY_ROOTFS=DSL

export PATH=$SNIC_NOBACKUP/software/VeloxChemMP/build/bin:$PATH
export PYTHONPATH=$SNIC_NOBACKUP/software/VeloxChemMP/build/python:$PYTHONPATH
export OMP_NUM_THREADS=32

job=tio2.495
srun -n 32 vlx ${job}.inp ${job}.out
```

### Benchmark cases

| System          | Input file                   | #nodes | Walltime [sec]| #core-hours  |
| :-------------- | :--------------------------- | -----: | ------------: | -----------: |
| Titanium oxide  | [tio2.495.inp](tio2.495.inp) |    32  | 44453.29      | 12644.49     |
| Insulin monomer | [insulin.inp](insulin.inp)   |     8  | 23448.95      | 1667.48      |
| Helicene        | [helicene.inp](helicene.inp) |     4  | 34159.64      | 1214.56      |