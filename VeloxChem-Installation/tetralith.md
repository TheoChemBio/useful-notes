# Tetralith

### Installation of VeloxChem

```
module load buildenv-intel/2018a-eb
module load Python/3.6.4-nsc2-intel-2018a-eb
module load git/2.19.1-nsc1-gcc-system

pip install --user h5py pybind11 pytest matplotlib

mkdir -p $HOME/software
cd $HOME/software

git clone https://gitlab.com/rinkevic/VeloxChemMP.git
cd VeloxChemMP
python3 config/generate_setup.py

export PATH=$HOME/software/VeloxChemMP/build/bin:$PATH
export PYTHONPATH=$HOME/software/VeloxChemMP/build/python:$PYTHONPATH

salloc -N 1 -t 30 -A snic2019-3-203 --reservation devel
OMP_NUM_THREADS=32 mpirun -n 1 python3 setup.py build

OMP_NUM_THREADS=32 mpirun -n 1 pytest -v python_tests
OMP_NUM_THREADS=16 mpirun -n 2 pytest -v python_tests
```

### Job submission script

```
#!/bin/bash

#SBATCH --job-name=tio2
#SBATCH --account=snic201X-X-XXX
#SBATCH --time=23:59:00

#SBATCH --nodes=32
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=32

module load buildtool-easybuild/3.5.3-nsc17d8ce4
module load intel/2018a
module load Python/3.6.4-nsc2-intel-2018a-eb

export PATH=$HOME/software/VeloxChemMP/build/bin:$PATH
export PYTHONPATH=$HOME/software/VeloxChemMP/build/python:$PYTHONPATH
export OMP_NUM_THREADS=32

job=tio2.495
mpirun -n 32 vlx ${job}.inp ${job}.out
```

### Benchmark cases

| System          | Input file                   | #nodes | Walltime [sec]| #core-hours  |
| :-------------- | :--------------------------- | -----: | ------------: | -----------: |
| Titanium oxide  | [tio2.495.inp](tio2.495.inp) |    32  | 33018.71      | 9391.99      |
| Insulin monomer | [insulin.inp](insulin.inp)   |     8  | 16690.68      | 1186.89      |
| Helicene        | [helicene.inp](helicene.inp) |     4  |               |              |