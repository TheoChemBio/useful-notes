# Other Platform

### Prerequisites 

+ C++ compiler (Intel, GNU, or Clang)
+ MPI library (IntelMPI, MPICH, or OpenMPI)
+ Math library (MKL or OpenBLAS)
+ Python3 (e.g. Anaconda3)

### Installation of OpenBLAS (if MKL is not available)

```
mkdir -p $HOME/software/openblas/src

cd $HOME/software/openblas/src
wget https://github.com/xianyi/OpenBLAS/archive/v0.3.4.tar.gz
tar xf v0.3.4.tar.gz

cd OpenBLAS-0.3.4
make TARGET=<your-architecture> CC=gcc FC=gfortran USE_OPENMP=1
make PREFIX=$HOME/software/openblas install

export OPENBLASROOT=$HOME/software/openblas
```

### Installation of VeloxChem

**_Note_**: _For installation of mpi4py on Cray system please refer to_
[_this page_](https://www.pdc.kth.se/software/software/mpi4py/crayos7/3.0.2/index_building.html).

```
cd $HOME/software

# before installing mpi4py, please make sure that mpicc is in PATH
pip install mpi4py --no-binary=mpi4py
pip install numpy h5py pybind11 pytest matplotlib

git clone https://gitlab.com/rinkevic/VeloxChemMP.git
cd VeloxChemMP
python3 config/generate_setup.py

export PATH=$HOME/software/VeloxChemMP/build/bin:$PATH
export PYTHONPATH=$HOME/software/VeloxChemMP/build/python:$PYTHONPATH

export OMP_NUM_THREADS=...
mpirun -n ... python3 setup.py build
mpirun -n ... pytest -v python_tests
```